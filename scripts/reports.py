#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import fnmatch
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


# set paths
CURDIR, _ = os.path.split(os.path.abspath(__file__))
PRJDIR = os.path.abspath(os.path.join(CURDIR, ".."))
DATDIR = os.path.join(PRJDIR, "data")
REPDIR = os.path.join(PRJDIR, "reports")

# select csv files
CSVFILES = fnmatch.filter(os.listdir(DATDIR), "*.csv")

print("Loading csv files...")
CSV = {
    csv[:-4]: pd.read_csv(os.path.join(DATDIR, csv), index_col="NUTS_ID")
    for csv in CSVFILES
}


col = "Y_2050"

# extract / transform
ap_fec = CSV["AP__private_cars__final_energy_consumption"].sort_values(
    by=col, ascending=False
)
ap_vstk = CSV["AP__private_cars__vehicle_stock"].sort_values(by=col, ascending=False)
cp_fec = CSV["CP__private_cars__final_energy_consumption"].sort_values(
    by=col, ascending=False
)
cp_vstk = CSV["CP__private_cars__vehicle_stock"].sort_values(by=col, ascending=False)

bau_fec = CSV["BaU__private_cars__final_energy_consumption"].sort_values(
    by=col, ascending=False
)
bau_fec_tot = CSV["BaU__total_transport__final_energy_consumption"].sort_values(
    by=col, ascending=False
)


index = index = bau_fec.index

fec = pd.DataFrame(
    dict(
        AP=ap_fec.loc[index, col],
        CP=cp_fec.loc[index, col],
        BaU=bau_fec.loc[index, col],
    ),
    index=index,
)

vstk = pd.DataFrame(
    dict(AP=ap_vstk.loc[index, col], CP=cp_vstk.loc[index, col]), index=index
)

bau = pd.DataFrame(
    dict(Total=bau_fec_tot.reindex(index)[col], Private=bau_fec.loc[index, col]),
    index=index,
)

# compare data and scenarios


##
## Private cars – Final Energy Consumption
##
sns.set(style="whitegrid")
sns.set_color_codes("pastel")

title = "Private cars – Final Energy Consumption"
print(f"Plotting: {title}")
fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(16, 8))
alin = axes[0]
abar = axes[1]

# lines
sns.scatterplot(
    x=index, y=fec.loc[index, "BaU"], label="BaU", color="r", ax=alin
)  # , sort=False) # if lineplot
sns.scatterplot(
    x=index, y=fec.loc[index, "CP"], label="CP", color="b", ax=alin
)  # , sort=False)
sns.scatterplot(
    x=index, y=fec.loc[index, "AP"], label="AP", color="g", ax=alin
)  # , sort=False)

alin.legend()
alin.set_title("Private cars – Final Energy Consumption")
alin.set_ylabel("ktoe")
# alin.set_yscale('log')

# bars
sns.barplot(x=index, y=vstk.loc[index, "CP"], label="CP", color="b")  # , ax=abar)
sns.barplot(x=index, y=vstk.loc[index, "AP"], label="AP", color="g")  # , ax=abar)

abar.legend()
abar.set_title(title)
abar.set_ylabel("numb. of vehicles")
# abar.set_yscale('log')

fig.savefig(
    os.path.join(REPDIR, "compare__private_cars.png"), tight_layout=True, dpi=300
)


##
## Buisness as Ususal – Final Energy Consumption
##
sns.set(style="whitegrid")
sns.set_color_codes("pastel")

title = "Buisness as Ususal – Final Energy Consumption"
print(f"Plotting: {title}")
fig, ax = plt.subplots(figsize=(16, 8))
sns.barplot(
    x=index, y=bau.loc[index, "Total"], label="Total transport", color="b"
)  # , ax=abar)
sns.barplot(
    x=index, y=bau.loc[index, "Private"], label="Private cars", color="r"
)  # , ax=abar)

ax.legend()
ax.set_title(title)
ax.set_ylabel("ktoe")
# ax.set_yscale('log')

fig.savefig(os.path.join(REPDIR, "compare__BaU.png"), tight_layout=True, dpi=300)
