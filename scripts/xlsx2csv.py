#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

import pandas as pd

CURDIR, _ = os.path.split(os.path.abspath(__file__))
PRJDIR = os.path.abspath(os.path.join(CURDIR, ".."))
DATDIR = os.path.join(PRJDIR, "data")
FNAME = "NUTS0_transport_data.xlsx"
FPATH = os.path.join(DATDIR, FNAME)


excel = pd.io.excel.ExcelFile(FPATH)
sheet_names = excel.sheet_names


csvnames = {
    "total_energy_demand": "BaU__total_transport__final_energy_consumption.csv",
    "passenger_energy_demand_private": "BaU__private_cars__final_energy_consumption.csv",
    "vehicle_stock_scenario_AP": "AP__private_cars__vehicle_stock.csv",
    "energy_demand_scenario_AP": "AP__private_cars__final_energy_consumption.csv",
    "vehicle_stock_scenario_CP": "CP__private_cars__vehicle_stock.csv",
    "energy_demand_scenario_CP": "CP__private_cars__final_energy_consumption.csv",
}

for sheet in sheet_names:
    csvname = csvnames[sheet]
    if csvname:
        print(f"{sheet} => {csvname}")
        df_sheet = pd.read_excel(excel, sheet_name=sheet)
        df_sheet.to_csv(os.path.join(DATDIR, csvname), index=False)
