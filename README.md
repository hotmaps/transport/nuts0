[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687634.svg)](https://doi.org/10.5281/zenodo.4687634)

# Vehiclestock at NUTS0 level


# Transport

Within Task *2.9 Transport* following data sets were developed and provided for the Hotmaps toolbox.

<table>
  <tr>
    <td>Parameters</td>
    <td>Space resolution</td>
    <td>Time resolution</td>
    <td>Data sources</td>
  </tr>
  <tr>
    <td>Vehicle stock and projections</td>
    <td>-</td>
    <td>yearly</td>
    <td>[1], [2]</td>
  </tr>
  <tr>
    <td>Electricity need for all transportation modes (electrified transport)</td>
    <td>georeferenced</td>
    <td>yearly</td>
    <td>[1], [2]</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.9 Transport.

This work include three main scenarios:
* Buisness as usual provided by [2], reference year 2016, for the **total vehicle stock**;
* Scenario CP – Current Policies (BAU): vehicle stock e final energy consumption for **private cars** in ktoe in 2050 [3, 4], reference year 2012;
* Scenario AP – Ambitious Policies (100% evs): vehicle stock e final energy consumption for **private cars** in ktoe in 2050 [3, 4], reference year 2012;


![alt text](reports/compare__BaU.png "Buisness as Usual – Final Energy Consumption: Total vs Private")


![alt text](reports/compare__private_cars.png "Compare scenarios on private cars – Final Energy Consumption")


## Methodology

The datasets (EU28 MSs, NUTS0) containing the time series for each parameter for the period from 1990 to 2050 were compiled based on the data as shown in Table 1. 
The historical data points were extrapolated based on future trends for selected indicators derived from the PRIMES –TREMOVE EU 2016 reference scenario [2] as described in [3, 4]. 


## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 

## Authors

Zubaryeva Alyona


## License


Copyright © 2016-2018: Alyona Zubaryeva

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.


## References

[1] Eurostat, “Database Information,” 2018. [Online]. Available: http://ec.europa.eu/eurostat/data/database/information

[2] European Commission, “EU Reference Scenario 2016.” [Online]. Available: https://data.europa.eu/euodp/data/dataset/energy-modelling

[3] Thiel, C., Drossinos, Y., Krause, J., Harrison, G., Gkatzoflias, D., Donati, A.V., 2016. Modelling Electro-mobility: An Integrated Modelling Platform for Assessing European Policies. Transp. Res. Procedia, Transport Research Arena TRA2016 14, 2544–2553. https://doi.org/10.1016/j.trpro.2016.05.341

[4] A. Zubaryeva, C. Thiel, N. Zaccarelli, E. Barbone, and A. Mercier, “Spatial multi-criteria assessment of potential lead markets for electrified vehicles in Europe,” Transp. Res. Part Policy Pract., vol. 46, no. 9, pp. 1477–1489, 2012
